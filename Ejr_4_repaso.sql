CREATE DATABASE IF NOT EXISTS Starswars;
USE Starswars;

CREATE TABLE Personajes(
    Codigo INTEGER PRIMARY KEY,
    Nombre VARCHAE(30),
    Raza VARCHAR(20),
    Grado VARCHAR(20),
    CodigoActor INTEGER REFERENCES Actores(Codigo)
    );
    
CREATE TABLE Actores(
    Codigo INTEGER PRIMARY KEY,
    Nombre VARCHAE(30),
    Fecha DATE,
    Nacionalidad VARCHAR(20)
);

ALTER TABLE Personajes ADD FOREGIN KEY(CodigoActor) REFERENCES Actores(Codigo) ON UPDATE CASCADE;