# Ejercicio 1
## Entrar desde la linea de comandos a MySQL con usuario root y sin que se vea la escritura de la contraseña
```sql
sudo mysql -u root -p
```
## Mirar las bases de datos que existen
```sql
SHOW DATABASES;
```
## Crear una nueva llamada METEO
```sql
CREATE DATABASE METEO;
```
## Usar METEO como base de datos de trabajo
```sql
USE METEO;
```
## Crear todas las tablas definidas anteriormente
```sql
CREATE TABLE Ciudad(
codigo SMALLINT UNSIGNED AUTO_INCREMENT, 
nombre VARCHAR(10) UNIQUE,
CONSTRAINT Ciudad_PK PRIMARY KEY(codigo)
);

CREATE TABLE Temperatura(
Dia DATE,
Codigo_CIUDAD  SMALLINT UNSIGNED, 
Grados DECIMAL(3,1), 
CONSTRAINT Temperatura_Ciudad_FK FOREIGN KEY (Codigo_CIUDAD) 
    REFERENCES Ciudad(Codigo) 
    ON DELETE CASCADE 
    ON UPDATE CASCADE),
CONSTRAINT Temperatura_PK PRIMARY KEY(Dia, Codigo_CIUDAD);

CREATE TABLE Humedad(
Dia DATE,
Codigo_CIUDAD SMALLINT UNSIGNED,
Porcentaje TINYINT UNSIGNED NOT NULL,
CONSTRAINT Humedad_Ciudad_FK FOREIGN KEY (Codigo_CIUDAD)
    REFERENCES Ciudad(Codigo) 
    ON DELETE CASCADE 
    ON UPDATE CASCADE),
CONSTRAINT Humedad_PK PRIMARY KEY(Dia, Codigo_CIUDAD);

CREATE TABLE Informe(
ID INT UNSIGNED AUTO_INCREMENT,
Dia DATE,
Codigo_CIUDAD SMALLINT UNSIGNED NOT NULL, 
CONSTRAINT Informe_PK PRIMARY KEY(ID),
CONSTRAINT Informe_Ciudad_FK FOREIGN KEY (Codigo_CIUDAD) 
    REFERENCES Ciudad(Codigo) 
    ON DELETE CASCADE 
    ON UPDATE CASCADE
);
```
## Insertar mínimo los datos de 2 ciudades con la información meteorológica de hoy
```sql
INSERT INTO Ciudad(Nombre) VALUES ('MADRID'), ('COSLADA'); -- TAMBIEN VALDRIA CON COMILLAS DOBLES

INSERT INTO Temperatura VALUES ('2020-01-10',1, 15 ), ('2020-01-10', 2, 20);

INSERT INTO Humedad VALUES ('2020-01-10',1, 20), ('2020-01-10', 2, 15);

INSERT INTO Informes(Dia, Codigo_CIUDAD) VALUES ('2020-01-10',1), ('2020-01-10',2);

```
# Ejercicio 2
* Indica los errores en la siguiente sintaxis y arréglalo *
```sql
CREATE TABLE tab1(
col1 INTEGER(5) PRIMARY KEY,
col2 CHAR(25) NOT NULL PRIMARY KEY,
col3 INT,
col4 VARCHAR(10)REFERENCES tab2(col4),
CONSTRAINT fk FOREIGN KEY (col3) REFERENCES tab2,
CONSTRAINT fk FOREIGN KEY (col4) REFERENCES tab2
);
```
El error reside en que se esta definiendo la primary key a nivel de columna,
lo que no permite multiclave, se tendria que hacer a nivel de tabla, 
ademas, la sintaxis de FOREIGN KEY es erronea.

arreglado quedaria de la siguiente forma:
```sql
CREATE TABLE tab1(
col1 INTEGER(5)
col2 CHAR(25),
col3 INT,
col4 VARCHAR(10)FOREIGN KEY REFERENCES tab2(col4),
CONSTRAINT fk1 FOREIGN KEY (col3) REFERENCES tab2(col3),
CONSTRAINT fk2 FOREIGN KEY (col4) REFERENCES tab2(col4),
PRIMARY KEY(col1,col2)
);
```
# Ejercicio 3
*  Teniendo que generar una base de datos de socios de una biblioteca con la siguiente información (Estando conectado NºLibro y NºSocio de la tabla PRESTAMOS con las otras): *

![Imagenenunciado](/ruta/a/la/imagen.jpg "Imagen Enunciado")

## Definir todas las tablas sin FK

## Añadir (a continuación del apartado anterior) que la  tabla PRESTAMOS tenga FK de las otras tablas

## Insertar los valores mostrados en el ejemplo

## Borrar el socio número 4. Explicar si sale un mensaje de aviso, un mensaje de error o no ocurre nada y porqué

## Mostrar el contenido de la tabña PRESTAMOS con ls columna IDPRESTAMOS como "Ordeb de Prestamos", NºSOCIO y NºLIBRO mostrando sólo los dos primeros.