# ***Ejercicios de refuerzo***

### *Tema 3*
### **por @Victor-bin**
# Indice

- [Ejercicio 1](#ejercicio-1)
  * [CLIENTES](#clientes)
  * [TIENDAS](#tiendas)
  * [OPERADORAS](#operadoras)
  * [TARIFAS](#tarifas)
  * [MOVILES](#moviles)
  * [MOVIL_LIBRE](#movil-libre)
  * [MOVIL_CONTRATO](#movil-contrato)
  * [OFERTAS](#ofertas)
  * [COMPRAS](#compras)
  * [CONTRATOS](#contratos)
- [Ejercicio 2](#ejercicio-2)
  * [ACTORES](#actores)
  * [PERSONAJES](#personajes)
  * [PLANETAS](#planetas)
  * [PELICULAS](#peliculas)
  * [NAVES](#naves)
  * [PERSONAJESPELICULAS](#personajespeliculas)
  * [VISITAS](#visitas)
- [Ejercicio 3](#ejercicio-3)

# Ejercicio 1

Se desea diseñar una base de datos para una conocida tienda de móviles. Para ello se dispondrá de las siguientes tablas para poder almacenarla.

## CLIENTES
```sql
CREATE TABLE CLIENTES (
DNI CHAR(9), 
Nombre VARCHAR(10), 
Apellidos VARCHAR(20), 
Telefono CHAR(9) UNSIGNED, 
Email VARCHAR(20),
PRIMARY KEY (DNI)
);
```

## TIENDAS
```sql
CREATE TABLE TIENDAS (
Nombre VARCHAR(10), 
Provincia VARCHAR(20), 
Localidad VARCHAR(20), 
Direccion VARCHAR(20), 
Telefono CHAR(9) UNSIGNED, 
DiaApertura ENUM('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'), 
DiaCierre  ENUM('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'), 
HoraApertura TIME, 
HoraCierre TIME,
PRIMARY KEY (Nombre)
);
```

## OPERADORAS
```sql
CREATE TABLE OPERADORAS (
Nombre VARCHAR(10),
ColorLogo VARCHAR(10),
PorcentajeCobertura TINYINT UNSIGNED COMMENT 'Unidades en %',
FrecuenciaGSM SMALLINT UNSIGNED,
PaginaWeb VARCHAR(50),
PRIMARY KEY (Nombre)
);
```

## TARIFAS
```sql
CREATE TABLE TARIFAS (
Nombre VARCHAR(10),
Nombre_OPERADORAS VARCHAR(10),
TamanyoDatos SMALLINT UNSIGNED,
TipoDatos ENUM ('Bytes','KiloBytes','MegaBytes','GigaBytes','TeraBytes'),
MinutosGratis TINYINT UNSIGNED,
SMSGratis TINYINT UNSIGNED,
FOREIGN KEY (Nombre_OPERADORAS) 
    REFERENCES OPERADORAS(Nombre) 
    ON DELETE CASCADE 
    ON UPDATE CASCADE,
PRIMARY KEY (Nombre, Nombre_OPERADORAS)
);
```

## MOVILES
```sql
CREATE TABLE MOVILES (
Marca VARCHAR(10), 
Modelo VARCHAR(10), 
Descripcion VARCHAR(50), 
SO VARCHAR(10), 
RAM TINYINT UNSIGNED, 
PulgadasPantalla TINYINT UNSIGNED,
CamaraMpx TINYINT UNSIGNED,
PRIMARY KEY (Marca, Modelo)
);
```

## MOVIL_LIBRE
```sql
CREATE TABLE MOVIL_LIBRE (
Marca_MOVILES VARCHAR(10), 
Modelo_MOVILES VARCHAR(10),
precio FLOAT(6,2) UNSIGNED,
FOREIGN KEY (Marca_MOVILES, Modelo_MOVILES) 
    REFERENCES MOVILES(Marca, Modelo) 
    ON DELETE CASCADE 
    ON UPDATE CASCADE,
PRIMARY KEY (Modelo_MOVILES)
);
```

## MOVIL_CONTRATO
```sql
CREATE TABLE MOVIL_CONTRATO (
Marca_MOVILES VARCHAR(10),
Modelo_MOVILES VARCHAR(10),
Nombre_OPERADORAS VARCHAR(10),
precio FLOAT(6,2) UNSIGNED,
FOREIGN KEY (Marca_MOVILES, Modelo_MOVILES) 
    REFERENCES MOVILES(Marca, Modelo) 
    ON DELETE CASCADE 
    ON UPDATE CASCADE,
FOREIGN KEY (Nombre_OPERADORAS) 
    REFERENCES OPERADORAS(Nombre) 
    ON DELETE CASCADE 
    ON UPDATE CASCADE,
PRIMARY KEY (Marca_MOVILES, Modelo_MOVILES, Nombre_OPERADORAS)
);
```

## OFERTAS
```sql
CREATE TABLE OFERTAS (
Nombre_OPERADORAS_TARIFAS VARCHAR(10),
Nombre_TARIFAS VARCHAR(10), 
Marca_MOVIL_CONTRATO VARCHAR(10), 
Modelo_MOVIL_CONTRATO VARCHAR(10),
FOREIGN KEY (Nombre_OPERADORAS_TARIFAS,Nombre_TARIFAS) 
    REFERENCES TARIFAS(Nombre_OPERADORAS, Nombre) 
    ON DELETE CASCADE 
    ON UPDATE CASCADE,
FOREIGN KEY (Marca_MOVIL_CONTRATO, Modelo_MOVIL_CONTRATO) 
    REFERENCES MOVIL_CONTRATO(Marca_MOVILES, Modelo_MOVILES) 
    ON DELETE CASCADE 
    ON UPDATE CASCADE,
PRIMARY KEY (Nombre_TARIFAS, Marca_MOVIL_CONTRATO, Modelo_MOVIL_CONTRATO)
);
```

## COMPRAS
```sql
CREATE TABLE COMPRAS (
DNI_CLIENTES CHAR(9), 
Nombre_TIENDAS VARCHAR(10),
Marca_MOVIL_LIBRE VARCHAR(10), 
Modelo_MOVIL_LIBRE VARCHAR(10),
Dia DATE,
FOREIGN KEY (DNI_CLIENTES) 
    REFERENCES CLIENTES(DNI) 
    ON DELETE CASCADE 
    ON UPDATE CASCADE,
FOREIGN KEY (Nombre_TIENDAS) 
    REFERENCES TIENDAS(Nombre) 
    ON DELETE CASCADE 
    ON UPDATE CASCADE,
FOREIGN KEY (Marca_MOVIL_LIBRE,Modelo_MOVIL_LIBRE) 
    REFERENCES MOVIL_LIBRE(Marca_MOVILES,Modelo_MOVILES) 
    ON DELETE CASCADE 
    ON UPDATE CASCADE,
PRIMARY KEY (DNI_CLIENTES, Nombre_TIENDAS, Marca_MOVIL_LIBRE, Modelo_MOVIL_LIBRE)
);
```

## CONTRATOS
```sql
CREATE TABLE CONTRATOS (
DNI_CLIENTES CHAR(9),
Nombre_TIENDAS VARCHAR(10),
Nombre_OPERADORAS_TARIFAS_OFERTAS VARCHAR(10),
Nombre_TARIFAS_OFERTAS VARCHAR(10),
Marca_MOVILES_OFERTAS VARCHAR(10),
Modelo_MOVILES_OFERTAS VARCHAR(10),
Dia DATE,
FOREIGN KEY (DNI_CLIENTES) 
    REFERENCES CLIENTES(DNI) 
    ON DELETE CASCADE 
    ON UPDATE CASCADE,
FOREIGN KEY (
    Nombre_OPERADORAS_TARIFAS_OFERTAS,
    Nombre_TARIFAS_OFERTAS,
    Marca_MOVILES_OFERTAS,
    Modelo_MOVILES_OFERTAS) 
REFERENCES OFERTAS(
    Nombre_OPERADORAS_TARIFAS,
    Nombre_TARIFAS,
    Marca_MOVIL_CONTRATO,
    Modelo_MOVIL_CONTRATO) 
ON DELETE CASCADE 
ON UPDATE CASCADE,
PRIMARY KEY (DNI_CLIENTES, Nombre_TIENDAS, Nombre_OPERADORAS_TARIFAS_OFERTAS, Nombre_TARIFAS_OFERTAS, Marca_MOVILES_OFERTAS, Modelo_MOVILES_OFERTAS)
);

```

# Ejercicio 2 

## ACTORES

```sql
CREATE TABLE ACTORES (
Codigo INT UNSIGNED AUTO_INCREMENT,
Nombre VARCHAR(10),
Fecha DATE,
Nacionalidad CHAR(2) COMMENT 'ISO-3166-1 ALPHA-2',
PRIMARY KEY (Codigo)
);
```
## PERSONAJES
```sql
CREATE TABLE PERSONAJES (
Codigo INT UNSIGNED AUTO_INCREMENT,
Nombre VARCHAR(10),
Raza VARCHAR(10),
Grado VARCHAR(10),
Codigo_ACTORES INT UNSIGNED,
CodigoSuperior_PERSONAJES INT
FOREIGN KEY (Codigo_ACTORES) 
    REFERENCES ACTORES(Codigo) 
    ON DELETE CASCADE 
    ON UPDATE CASCADE,
PRIMARY KEY (Codigo),
FOREIGN KEY (CodigoSuperior_PERSONAJES) 
    REFERENCES PERSONAJES(Codigo) 
    ON DELETE CASCADE 
    ON UPDATE CASCADE
);
```
## PLANETAS
```sql
CREATE TABLE PLANETAS (
Codigo INT UNSIGNED AUTO_INCREMENT,
Galaxia VARCHAR(10),
Nombre VARCHAR(10),
PRIMARY KEY (Codigo)
);
```
## PELICULAS
```sql
CREATE TABLE PELICULAS (
Codigo INT UNSIGNED AUTO_INCREMENT,
Titulo VARCHAR(10),
Director VARCHAR(10),
Anyo YEAR,
PRIMARY KEY (Codigo)
);
```
## NAVES
```sql
CREATE TABLE NAVES (
Codigo INT UNSIGNED AUTO_INCREMENT,
NTripulantes INT UNSIGNED,
nombre VARCHAR(10),
PRIMARY KEY (Codigo)
);
```
## PERSONAJESPELICULAS
```sql
CREATE TABLE PERSONAJESPELICULAS (
Codigo_PERSONAJES INT UNSIGNED,
Codigo_PELICULAS INT UNSIGNED,
FOREIGN KEY (Codigo_PERSONAJES) 
    REFERENCES PERSONAJES(Codigo) 
    ON DELETE CASCADE 
    ON UPDATE CASCADE,
FOREIGN KEY (Codigo_PELICULAS) 
    REFERENCES PELICULAS(Codigo) 
    ON DELETE CASCADE 
    ON UPDATE CASCADE,
PRIMARY KEY (Codigo_PERSONAJES,Codigo_PELICULAS)
);
```
## VISITAS
```sql
CREATE TABLE VISITAS (
Codigo_NAVES INT UNSIGNED,
Codigo_PLANETAS INT UNSIGNED,
Codigo_PELICULAS INT UNSIGNED,
FOREIGN KEY (Codigo_NAVES) 
    REFERENCES NAVES(Codigo) 
    ON DELETE CASCADE 
    ON UPDATE CASCADE,
FOREIGN KEY (Codigo_PLANETAS) 
    REFERENCES PLANETAS(Codigo) 
    ON DELETE CASCADE 
    ON UPDATE CASCADE,
FOREIGN KEY (Codigo_PELICULAS) 
    REFERENCES PELICULAS(Codigo) 
    ON DELETE CASCADE 
    ON UPDATE CASCADE,
PRIMARY KEY (Codigo_NAVES,Codigo_PLANETAS,Codigo_PELICULAS)
);
```

# Ejercicio 3
1.	Usar los comandos de IF EXISTS, IF NOT EXISTS
2.	Todos los campos PK tienen que ser incrementarse automáticamente
```sql
    ALTER TABLE IF EXISTS ACTORES MODIFY Codigo INT UNSIGNED AUTO_INCREMENT;
```
3.	Año tiene que ser de tipo YEAR
```sql
    ALTER TABLE IF EXISTS PELICULAS MODIFY Anyo YEAR;
```
4.	Todos los campos relacionados tienen que declararse como FOREIGN KEY (FK)
```sql
    ALTER TABLE IF EXISTS VISITAS ADD FOREIGN KEY (Codigo_PLANETAS) 
        REFERENCES PLANETAS(Codigo) 
        ON DELETE CASCADE 
        ON UPDATE CASCADE;
```
5.	El charset que soporte la ñ y los acentos
```sql
    ALTER TABLE PELICULAS CONVERT TO CHARACTER SET utf8;
```
6.	Todas las tablas cuando se actualizan claves relacionadas se modifique automáticamente en todas
```sql
        ALTER TABLE IF EXISTS PELICULAS
        MODIFY FOREIGN KEY (Codigo_PERSONAJES)
        REFERENCES PERSONAJES (Codigo) ON UPDATE CASCADE;
        ;
```
7.	Todas las tablas cuando se borran claves relacionadas se modifique automáticamente a nulo
```sql
	    ALTER TABLE IF EXISTS  PELICULAS
        MODIFY FOREIGN KEY (Codigo_PERSONAJES)
        REFERENCES PERSONAJES (Codigo) ON DELETE CASCADE SET NULL;
```
8.	Eliminar el campo CodigoSuperior de la tabla Personajes
```sql
        ALTER TABLE IF EXISTS PERSONAJES DROP COLUMN CodigoSuperior;
```
9.	Añadir LadoFuerza en la tabla Personajes, donde puede tener sólo valores de 'Jedi' o 'Sith'
```sql
        ALTER TABLE IF EXISTS PERSONAJES ADD LadoFuerza ENUM('Jedi','Sith');
```
10.	Cargar otro archivo sólo con los datos a rellenar (vuestro)
```sql
    SOURCE starwars.sql;
```
11.	Actualizar algún valor de la tabla no clave
```sql
    UPDATE ACTORES SET  Nacionalidad = ES WHERE Codigo = 1;
```
12.	Actualizar algún valor de la tabla clave principal
```sql
    UPDATE ACTORES SET  Codigo = 2 WHERE Codigo = 1;
```
13.	Actualizar algún valor de la tabla clave foránea
```sql
    UPDATE PERSONAJES SET  Codigo_ACTORES = 2 WHERE Codigo = 1;
```
14.	Borrar algún valor de la tabla no clave
```sql
    UPDATE ACTORES SET  Nacionalidad = 'NULL' WHERE Codigo = 1;
```
15.	Borrar algún valor de la tabla clave principal
    
    NO SE PUEDE

16.	Borrar algún valor de la tabla clave foránea
	    
    NO SE PUEDE

17.	Borrar la tabla Visitas
```sql
    DROP TABLE IF EXISTS VISITAS CASCADE;
```
18.	Borrar la tabla Actores.
```sql
    DROP TABLE IF EXISTS ACTORES CASCADE;
 ```